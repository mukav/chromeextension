function getword(info,tab) {
  console.log("Word " + info.selectionText + " was clicked.");
  chrome.tabs.create({  
    url: "http://sci-hub.se/" + info.selectionText
  });
}
function scholar(info,tab) {
  console.log("Word " + info.selectionText + " was clicked.");
  chrome.tabs.create({  
    url: "https://scholar.google.ru/scholar?hl=ru&as_sdt=0%2C5&q=" + encodeURI(info.selectionText) +  "&btnG="
  });
}
function doi2bib(info,tab) {
  console.log("Word " + info.selectionText + " was clicked.");
  chrome.tabs.create({  
    url: "https://www.doi2bib.org/bib/" + encodeURIComponent(info.selectionText)
  });
}
chrome.contextMenus.create({
  title: "Open on scihub: %s", 
  contexts:["selection"], 
  onclick: getword
});
chrome.contextMenus.create({
  title: "Open on google cholar: %s", 
  contexts:["selection"], 
  onclick: scholar
});

chrome.contextMenus.create({
  title: "get bib by doi: %s", 
  contexts:["selection"], 
  onclick: doi2bib
});
