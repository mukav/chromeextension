$(function(){
	var sci_check = function(){
        var a = $( "#doi-url" );
        var doi = a.text();
        var as = "";
        console.log("doi: " + doi);
		if( doi != "" ){
            as = doi.split("/");
            a.after(" sci: <a href=\"https://sci-hub.se/" + as[as.length -2] + "/" + as[as.length - 1] + "\">sci-hub</a>");
		}
		else {
			setTimeout(sci_check, 1000); // check again in a second
		}
	}

	sci_check();
});
