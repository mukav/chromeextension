$(function(){
	var sci_check = function(){
        var doi = $( ".stats-document-abstract-doi" ).find( "a" ).text();
        console.log("doi: " + doi);
		if( doi != ""){
        	$( ".stats-document-abstract-doi" ).find( "a" ).after(" sci: <a href=\"https://sci-hub.se/" + doi +"\">sci-hub</a>");
		}
		else {
			setTimeout(sci_check, 1000); // check again in a second
		}
	}

	sci_check();

});
