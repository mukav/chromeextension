$(function(){
	var sci_check = function(){
        var a = $("span:contains('doi>')").find("a");
        var doi = a.text();
        console.log("doi: " + doi);
		if( doi != ""){
            a.after(" sci: <a href=\"https://sci-hub.se/" + doi +"\">sci-hub</a>");
		}
		else {
			setTimeout(sci_check, 1000); // check again in a second
		}
	}

	sci_check();
});
